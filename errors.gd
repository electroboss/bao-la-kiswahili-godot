extends Node

class ErrorClass:
	var error: ErrorType
	var error_args: Dictionary
	var error_str: String
	func _init(error: ErrorType, error_args: Dictionary={}):
		self.error = error
		self.error_args = error_args
		var errortype_str = ERROR_STRINGS[error]
		self.error_str = errortype_str + str(error_args)

class Result:
	var is_error: bool
	var error: ErrorClass
	var value
	
	func _init(value, is_error: bool=false) -> void:
		self.is_error = is_error
		if is_error:
			self.error = value
		else:
			self.value = value

enum ErrorType {
	BoardAddressError,
	GameLoopTooLongError
}
const ERROR_STRINGS = {
	ErrorType.BoardAddressError: "BoardAddressError",
	ErrorType.GameLoopTooLongError: "GameLoopTooLongError"
}

func err(error: ErrorType, args: Dictionary):
	return Result.new(ErrorClass.new(error, args), true)

func unwrap(result: Result):
	if result.is_error:
		print("ErrorClass: "+result.error.error_str)
		assert(false)
	else:
		return result.value
